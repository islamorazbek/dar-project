import React, { useEffect, useState } from 'react'
import axios from 'axios';
import { Article } from '../../shared/types';
import { useParams } from 'react-router-dom';
import ArticleCard from '../../components/card/ArticleCard';
import { getArticles } from '../../shared/api';

const ArticlesPage: React.FC = () => {

    const [articles, setArticles] = useState<Article[]>([]);
    const { categoryId } = useParams<{ categoryId: string }>();

    useEffect(() => {
        console.log(categoryId)
        getArticles(categoryId)
            .then(res => setArticles(res))
            .catch(err => console.log(err));
    }, [categoryId])


    return (
        <div >
            <header>
                <h1>
                    DAR News
                </h1>
            </header>
            <div style={{ width: '100vw', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                {articles.length ? articles.map((item, idx) => {
                    return (
                        <ArticleCard
                            key={idx}
                            article={articles[idx]}
                        />
                    )
                })
                    : <span>
                        Загрузка...
                    </span>
                }
            </div>
        </div>
    )
}

export default ArticlesPage;
