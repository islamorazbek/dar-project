import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import CommentForm from '../../components/comment-form/CommentForm';
import { getArticle } from '../../shared/api';
import { Article } from '../../shared/types';
import styles from './ArticlePage.module.scss';

const ArticlePage: React.FC = () => {

    const {articleId} = useParams<{articleId: string}>();

    const [article, setArticle] = useState<Article | null>(null);

    console.log(articleId)

    useEffect(() => {
        if (articleId) {
            getArticle(articleId)
                .then(res => setArticle(res.data))
        }
    }, [articleId])


    return (
        <div className="ArticlePage">
            {
                article && (
                    <div className={styles.article_content}>
                        <div className={styles.article_content_title}>
                            {article.title}
                        </div>
                        <div 
                            className={styles.article_content_annotation} 
                            dangerouslySetInnerHTML={{__html: article.description}}>
                        </div>
                        <CommentForm />
                    </div>
                    
                )
            }
        </div>
    )
}

export default ArticlePage;
