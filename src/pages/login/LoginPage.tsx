import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import Button from '../../components/button/Button';
import LoginForm from '../../components/login-form/LoginForm';
import RegisterForm from '../../components/register-form/RegisterForm';
import AppContext, { ActionTypes } from '../../shared/app.context';
import styles from './LoginPage.module.scss';

const LoginPage: React.FC = () => {

    const [autorization, setAutorization] = useState<'login' | 'registration'>('login');
    const [loginForm, setLoginForm] = useState({ username: '', password: '' });
    const [registrationForm, setRegistrationForm] = useState({
        username: '',
        email: '',
        firstName: '',
        lastName: '',
        password: '',
        confirmation: ''
    });

    const history = useHistory();
    const { state, dispatch } = useContext(AppContext)

    const login = () => {
        if (autorization === 'login') {
            localStorage.setItem('username', loginForm.username)
            dispatch({ type: ActionTypes.SET_PROFILE, payload: loginForm })
            history.push('/articles')
        } else {
            setAutorization('login');
        }
    }

    const register = () => {
        if (autorization === 'registration') {
            if (registrationForm.confirmation === registrationForm.password) {
                localStorage.setItem('username', registrationForm.username)
                dispatch({ type: ActionTypes.SET_PROFILE, payload: registrationForm })
                history.replace('/articles')
            }
        } else {
            setAutorization('registration');
        }
    }

    return (
        <div className={styles.wrapper}>
            <span className={styles.title}>Log-In</span>
            <div className={styles.controls}>
                {autorization === 'login' ?
                    <LoginForm loginForm={loginForm} setLoginForm={setLoginForm} />
                    :
                    <RegisterForm registrationForm={registrationForm} setRegistrationForm={setRegistrationForm} />
                }
            </div>
            <div className={styles.buttons}>
                <Button title="Register" variant="ghost" onClick={register} />
                <Button title="Login" variant="primary" onClick={login} />
            </div>
        </div>
    )
}

export default LoginPage
