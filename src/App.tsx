import React, { useEffect, useReducer } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import './App.scss';
import Counter from './components/counter/Counter';
import Header from './components/header/Header';
import ArticlePage from './pages/article/ArticlePage';
import ArticlesPage from './pages/articles/ArticlesPage';
import LoginPage from './pages/login/LoginPage';
import AppContext, { ActionTypes, reducer } from './shared/app.context';

const App: React.FC = () => {

  const [state, dispatch] = useReducer(reducer, {
    profile: null,
  })

  useEffect(() => {
    if (localStorage.getItem('username')?.length) {
      dispatch({ type: ActionTypes.SET_PROFILE, payload: { username: localStorage.getItem('username') } })
    }
  }, [state])

  return (
    <AppContext.Provider value={{ state, dispatch }}>
      <div className="App">
        <Header />
        <Switch>
          <Route exact path="/counter" component={Counter} />
          <Route path="/login" component={LoginPage} />
          <Route exact path="/articles" component={ArticlesPage} />
          <Route path="/articles/:categoryId" component={ArticlesPage} />
          <Route path="/article/:articleId" component={ArticlePage} />
          <Route path="*" render={() => <Redirect to="/articles" />} />
        </Switch>
      </div>
    </AppContext.Provider>

  );
}

export default App;
