import React from 'react';
import Button from '../button/Button';
import styles from './Counter.module.scss'
import useCounter from '../../hooks/useCounter';

const Counter: React.FC = () => {

    const { count, increment, decrement, refresh } = useCounter();

    return (
        <div className={styles.Counter}>

            <div className={styles.Counter__count}>
                {count}
            </div>

            <Button title="-" variant="ghost" onClick={decrement} />
            <Button title="Refresh" variant="ghost" onClick={refresh} />
            <Button title="+" variant="ghost" onClick={increment} />
        </div>
    )
}

export default Counter
