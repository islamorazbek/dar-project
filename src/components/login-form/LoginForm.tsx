import React, { useEffect, useState } from 'react';
import { Profile } from '../../shared/types';
import styles from './LoginForm.module.scss';

type Props = {
    loginForm: Profile,
    setLoginForm: (a: any) => void
}

const LoginForm: React.FC<Props> = ({ loginForm, setLoginForm }) => {

    const [error, setError] = useState<string | null>(null)

    useEffect(() => {
        if (loginForm.password.length < 6) {
            setError('Password must have at least 6 characters');
        } else {
            setError(null);
        }
    }, [loginForm.password])

    return (
        <>
            <input
                className={styles.input}
                value={loginForm.username}
                placeholder={'Enter your username'}
                type='text'
                name='username'
                onChange={(e) => setLoginForm((state: any) => ({ ...state, username: e.target.value }))}
            />
            <input
                className={styles.input}
                value={loginForm.password}
                placeholder={'Enter your password'}
                type='password'
                name='password'
                onChange={(e) => setLoginForm((state: any) => ({ ...state, password: e.target.value }))}
            />
            {loginForm.password.length && error?.length ?
                <span>{error}</span>
                :
                null
            }
        </>
    )
}

export default LoginForm
