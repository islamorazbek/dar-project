import React from 'react'
import { Profile } from '../../shared/types'
import HeaderBottom from './header-bottom/HeaderBottom'
import HeaderTop from './header-top/HeaderTop'

const Header: React.FC = () => {
    return (
        <div>
            <HeaderTop />
            <HeaderBottom />
        </div>
    )
}

export default Header
