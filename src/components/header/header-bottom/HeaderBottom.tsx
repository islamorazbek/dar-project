import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { getCategories } from '../../../shared/api'
import { Category } from '../../../shared/types'
import './HeaderBottom.scss'

const HeaderBottom: React.FC = () => {

    const [categories, setCategories] = useState<Category[]>([])

    useEffect(() => {
        getCategories()
            .then(res => {
                setCategories(res.data);
            })
    }, [])

    return (
        <div className="Navigation">
            <div className="Navigation__inner">
                {categories.length > 0 &&
                    <>
                        <Link className="Navigation__inner__link" to="/articles">Все статьи</Link>
                        {categories.map((category, idx) => {
                            return (
                                <Link key={idx} className="Navigation__inner__link" to={`/articles/${category.id}`}>{category.title}</Link>
                            )
                        })}
                        <Link className="Navigation__inner__link" to="/counter">Counter</Link>
                    </>
                }
            </div>
        </div>
    )
}

export default HeaderBottom
