import React, { useContext, useEffect } from 'react';
import styles from './HeaderTop.module.scss'
import apple from '../../../assets/logos/apple.png'
import Button from '../../button/Button';
import { Link } from 'react-router-dom';
import AppContext, { ActionTypes, AppAction } from '../../../shared/app.context';

const HeaderTop: React.FC = () => {
    const { state, dispatch } = useContext(AppContext)
    const { profile } = state;

    const logout = () => {
        localStorage.clear();
        dispatch({ type: ActionTypes.RESET_PROFILE, payload: null })
    }

    return (
        <div className={styles.Header}>
            <img alt="logo" src={apple} className={styles.Header__logo} />

            <div className={styles.Header__navbar}>
                <a className={styles.Header__navbar__link} href="default.asp">О нас</a>
                <a className={styles.Header__navbar__link} href="default.asp">Обучение</a>
                <a className={styles.Header__navbar__link} href="default.asp">Сообщество</a>
                <a className={styles.Header__navbar__link} href="default.asp">Медиа</a>
            </div>

            {profile ?
                <Link to={'/user'} style={{ textDecoration: 'none', color: 'black' }}>
                    <div className={styles.userArea}>
                        <span className={styles.userName}>{profile.username}</span>
                        <Button title="Log out" variant="primary" onClick={() => logout()} />
                    </div>
                </Link>
                :
                <Link to="/login" style={{ textDecoration: 'none' }}>
                    <Button title="Log In" variant="primary" onClick={() => console.log("")} />
                </Link>

            }

        </div >
    )
}

export default HeaderTop
