import React from 'react';
import { Link } from 'react-router-dom';
import { Article } from '../../shared/types';
import styles from './ArticleCard.module.scss';

type Props = {
    article: Article
}

const ArticleCard: React.FC<Props> = ({ article }) => {

    return (
        <Link to={`/article/${article.id}`} style={{textDecoration: 'none', marginBottom: '10px'}}>
            <div className={styles.Card}>
                <div className={styles.RunningTitle}>
                    <span className={styles.GhostText}>
                        {article.id}
                    </span>
                    <span className={styles.GhostText}>
                        Автор: {article.author.name}
                    </span>
                </div>
                <span className={styles.Title}>
                    {article.title}
                </span>
                <div className={styles.RunningTitle}>
                    <span className={styles.GhostText}>
                        Категория: {article.category.title}
                    </span>
                    <span className={styles.GhostText}>
                        {article.updated_at}
                    </span>
                </div>
            </div>
        </Link>

    )
}

export default ArticleCard
