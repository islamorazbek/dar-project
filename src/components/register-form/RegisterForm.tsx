import React, { useEffect, useState } from 'react'
import { Profile } from '../../shared/types'
import styles from './RegisterForm.module.scss'

type Props = {
    registrationForm: Profile,
    setRegistrationForm: (a: any) => void
}

const RegisterForm: React.FC<Props> = ({ registrationForm, setRegistrationForm }) => {

    const [error, setError] = useState<string | null>(null)

    useEffect(() => {
        if (registrationForm.password.length < 6) {
            setError('Password must have at least 6 characters');
        } else {
            setError(null);
        }
    }, [registrationForm.password])

    useEffect(() => {
        if (registrationForm.password !== registrationForm.confirmation) {
            setError('Confirmation is wrong');
        } else {
            setError(null);
        }
    }, [registrationForm.confirmation])

    return (
        <>
            <input
                className={styles.input}
                value={registrationForm.username}
                placeholder={'Enter your username'}
                type='text'
                name='username'
                onChange={(e) => setRegistrationForm((state: any) => ({ ...state, username: e.target.value }))}
            />
            <input
                className={styles.input}
                value={registrationForm.username}
                placeholder={'Enter your email'}
                type='text'
                name='email'
                onChange={(e) => setRegistrationForm((state: any) => ({ ...state, email: e.target.value }))}
            />
            <input
                className={styles.input}
                value={registrationForm.username}
                placeholder={'Enter your First Name'}
                type='text'
                name='firstname'
                onChange={(e) => setRegistrationForm((state: any) => ({ ...state, firstName: e.target.value }))}
            />
            <input
                className={styles.input}
                value={registrationForm.username}
                placeholder={'Enter your Last Name'}
                type='text'
                name='lastname'
                onChange={(e) => setRegistrationForm((state: any) => ({ ...state, lastName: e.target.value }))}
            />
            <input
                className={styles.input}
                value={registrationForm.password}
                placeholder={'Enter your password'}
                type='password'
                name='password'
                onChange={(e) => setRegistrationForm((state: any) => ({ ...state, password: e.target.value }))}
            />
            <input
                className={styles.input}
                value={registrationForm.confirmation}
                placeholder={'Confirm your password'}
                type='password'
                name='password'
                onChange={(e) => setRegistrationForm((state: any) => ({ ...state, confirmation: e.target.value }))}
            />
            {registrationForm.password.length && error?.length ?
                <span>{error}</span>
                :
                null
            }
        </>
    )
}

export default RegisterForm
