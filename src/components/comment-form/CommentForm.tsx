import React, { useContext, useEffect, useState } from 'react'
import AppContext from '../../shared/app.context';
import Button from '../button/Button';
import styles from './CommentForm.module.scss';

const CommentForm: React.FC = () => {

    const { state } = useContext(AppContext);
    const { profile } = state;

    const [fields, setFields] = useState<{
        username: string;
        email: string;
        comment: string;
    }>({
        username: '',
        email: '',
        comment: ''
    });

    const [errors, setErrors] = useState<{
        username: string | null;
        email: string | null;
        comment: string | null;
    }>({
        username: null,
        email: null,
        comment: null
    });

    const onSubmit = () => {
        console.log('Form submitted', fields)
    }

    function validateEmail(email: string) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    useEffect(() => {
        if (errors['username']?.length && fields['username'].length === 0) {
            setErrors(oldState => ({
                ...oldState,
                ['username']: 'Username cannot be empty!!!'
            }))
        } else {
            setErrors(oldState => ({
                ...oldState,
                ['username']: ''
            }))
        }
    }, [fields.username])

    useEffect(() => {
        if (errors['email']?.length && !validateEmail(fields['email'])) {
            setErrors(oldState => ({
                ...oldState,
                ['email']: 'Email format is wrong!!!'
            }))
        } else {
            setErrors(oldState => ({
                ...oldState,
                ['email']: ''
            }))
        }
    }, [fields.email])

    useEffect(() => {
        if (errors['comment']?.length && fields['comment'].length < 30) {
            setErrors(oldState => ({
                ...oldState,
                ['comment']: 'Comment part should have more than 30 characters!!!'
            }))
        } else {
            setErrors(oldState => ({
                ...oldState,
                ['comment']: ''
            }))
        }
    }, [fields.comment])

    const fieldChange = (fieldName: string, value: any) => {
        setFields(oldState => ({
            ...oldState,
            [fieldName]: value
        }));
        setErrors(oldState => ({
            ...oldState,
            [fieldName]: '   '
        }));
    }

    return (
        <div className={styles.wrapper}>
            <div className={styles.user}>
                <div className={styles.formControl}>
                    <label>Username</label>
                    {profile ?
                        <input value={profile.username} disabled />
                        :
                        < input value={fields.username} name="username" type="text" onChange={e => fieldChange('username', e.target.value)} />
                    }
                    {errors['username'] && <span style={{ color: 'red' }}>{errors['username']}</span>}
                </div>
                <div className={styles.formControl}>
                    <label>Email</label>
                    {profile ?
                        <input value={profile.email} disabled />
                        :
                        <input name="email" type="text" value={fields.email} onChange={e => fieldChange('email', e.target.value)} />

                    }
                    {errors['email'] && <span style={{ color: 'red' }}>{errors['email']}</span>}
                </div>
            </div>
            <div className={styles.comment}>
                <div className={styles.formControl}>
                    <label>Comment</label>
                    <textarea name="comment" value={fields.comment} onChange={e => fieldChange('comment', e.target.value)} />
                    {errors['comment'] && <span style={{ color: 'red' }}>{errors['comment']}</span>}
                </div>
            </div>
            <div className={styles.controls}>
                <Button title="Submit" variant="primary" onClick={onSubmit} />
            </div>
        </div>
    )
}

export default CommentForm
