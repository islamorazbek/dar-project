import React from 'react';
import styled from 'styled-components';

type StyledButtonProps = {
    outlined?: boolean;
}

const StyledButton = styled.button<StyledButtonProps>`
    padding: 12px 30px;
    text-transform: uppercase;
    border: solid 0.5px blueviolet;
    background-color: ${props => props.outlined ? 'none' : 'blueviolet'};
    color: ${props => props.outlined ? 'blueviolet' : 'white'};
    cursor: pointer;
    &:hover {
        color: blueviolet;
        border: solid 0.5px blueviolet;
        background-color: white;
    }
`
const SecondaryButton = styled(StyledButton)`
    border: solid 0.5px rosybrown;
    background-color: ${props => props.outlined ? 'none' : 'rosybrown'};
    color: ${props => props.outlined ? 'rosybrown' : 'white'};
`

const GhostButton = styled(StyledButton)`
    border: solid 0.5px blueviolet;
    color: blueviolet;
    background-color: white;
    &:hover {
        color: white;
        border: solid 0.5px white;
        background-color: blueviolet;
    }
`

type Props = {
    title: string;
    outlined?: boolean; // ? - for optional case
    variant?: 'primary' | 'secondary' | 'ghost';
    onClick: () => void;
}

const getStyledButton = (variant: 'primary' | 'secondary' | 'ghost') => {
    switch (variant) {
        case 'primary':
            return StyledButton
        case 'secondary':
            return SecondaryButton
        case 'ghost':
            return GhostButton
        default:
            return StyledButton
    }
}

const Button: React.FC<Props> = ({ title, outlined = false, variant, onClick }) => {
    const StyledButtonComponent = getStyledButton(variant || 'primary');

    return (
        <StyledButtonComponent onClick={onClick}>
            {title}
        </StyledButtonComponent>
    )
}

export default Button
